import { Injectable } from '@nestjs/common';
import axios from 'axios';
import * as https from 'https';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  createWorkflow(workflow: string): string {
    const data = JSON.stringify(workflow);
    console.log(data);

    const config = {
      method: 'post',
      maxBodyLength: Infinity,
      url: 'https://10.100.61.199:2746/api/v1/workflows/argo',
      headers: {
        'Content-Type': 'application/json',
      },
      data: data,
      httpsAgent: new https.Agent({
        rejectUnauthorized: false,
      }),
    };

    axios
      .request(config)
      .then((response) => {
        console.log(JSON.stringify(response.data));
      })
      .catch((error) => {
        console.log(error);
      });

    return 'Hello World!';
  }
}
